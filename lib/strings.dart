//// General strings
const String appName = "SchoolFix";
const String appSlogan = "Fix. Your. School.";

//// Prefs strings
const String prefsSchoolName = "schoolName";
const String prefsSchoolAddress = "schoolAddress";
const String prefsSchoolId = "schoolId";

//// Firestore
const String fsSchoolsCollection = "schools";

//// Other
const String enterSchoolName = "Inserisci il nome della tua scuola";

// Se nessuno non ha scannerizzatto il qr, mostra l'avviso di stamparlo e incollarlo nella scuola.