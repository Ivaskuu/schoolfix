import 'package:flutter/material.dart';
import 'screens/login/ui_login.dart';
import 'classes/school.dart';
import 'package:shared_preferences/shared_preferences.dart';

SharedPreferences prefs;
String schoolName = "...";
String schoolAddress = "...";
String schoolID = "...";

School school;

void main()
{
  _loadPrefs();
  
  runApp
  (
    new MaterialApp
    (
      theme: new ThemeData
      (
        primaryColor: Colors.deepPurple,
        accentColor: Colors.deepPurpleAccent,
      ),
      title: "SchoolFix",
      home: new UILogin(),
    )
  );
}

_loadPrefs() async
{
  prefs = await SharedPreferences.getInstance();

  if(prefs.getString("schoolName") != null)
  {
    schoolName = prefs.getString("schoolName");
    schoolAddress = prefs.getString("schoolAddress");
    schoolID = prefs.getString("schoolId");

    school = new School(schoolName, schoolAddress, schoolID);
  }
}