import 'package:flutter/material.dart';
import 'dart:async';

import 'package:url_launcher/url_launcher.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../ui_main.dart';
import '../../main.dart' as main;
import '../../strings.dart' as strings;
import '../../classes/school.dart';
import '../../classes/problem.dart';
import '../../classes/solution.dart';
import 'ui_add_new_school.dart';
import 'ui_onboarding.dart';

List<School> schools = new List<School>();

class UILogin extends StatefulWidget
{
  final List<AssetImage> images =
  [
    new AssetImage("img/school1.jpg"),
    new AssetImage("img/school2.jpg"),
    new AssetImage("img/school3.jpeg"),
    new AssetImage("img/school4.jpg"),
    new AssetImage("img/school5.jpeg")
  ];

  @override
  _UILoginState createState() => new _UILoginState();
}

class _UILoginState extends State<UILogin>
{
  int actualImg = 0;
  bool scanned = false;
  TextEditingController _schoolNameController = new TextEditingController();
  Timer _changeImgsTimer;
  Timer _checkSchoolExistsTimer;
  int _numbersOfTry = 0; // For each try increase the wait time (connections error...)

  @override
  void initState()
  {
    super.initState();

    _checkLoggedIn();
    //Firestore.instance.collection('books').document().setData({ 'title': 'title', 'author': 'author' }); // Adding a new document
  }
  
  _checkLoggedIn()
  {
    SharedPreferences.getInstance().then((prefs)
    {
      print(prefs.getString(strings.prefsSchoolName));

      if(prefs.getString(strings.prefsSchoolName) != null)
      {
        Navigator.pushReplacement(context, new MaterialPageRoute(builder: (_) => new UIMain()));
      }
      else 
      {
        // Change the image
        _changeImgsTimer = new Timer.periodic
        (
          new Duration(seconds: 4), (Timer t)
          {
            setState(()
            {
              actualImg++;
              if(actualImg == widget.images.length) actualImg = 0;
            });
          }
        );
      }
    });
  }

  @override
  Widget build(BuildContext context)
  {
    return !scanned ? _loginPage() : _schoolSplashScreen();

    /*return new Scaffold
    (
      body: new StreamBuilder<QuerySnapshot>
      (
        stream: Firestore.instance.collection('books').snapshots,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot)
        {
          if (!snapshot.hasData) return new Center(child: new Text('Loading...'));

          return new ListView
          (
            children: snapshot.data.documents.map((DocumentSnapshot document)
            {
              return new ListTile
              (
                title: new Text(document['title']),
                subtitle: new Text(document['author']),
              );
            }).toList(),
          );
        },
      )
    );*/
  }

  Widget _loginPage()
  {
    return new Scaffold
    (
      body: new Stack
      (
        fit: StackFit.expand,
        children: <Widget>
        [
          new DecoratedBox
          (
            decoration: new BoxDecoration
            (
              image: new DecorationImage
              (
                image: widget.images[actualImg],
                fit: BoxFit.cover
              )
            ),
          ),
          new Align
          (
            alignment: FractionalOffset.center,
            child: new Container
            (
              color: Colors.black87,
              padding: new EdgeInsets.symmetric(horizontal: 32.0, vertical: 16.0),
              child: new Column
              (
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>
                [
                  new Text(strings.appName, style: new TextStyle(color: Colors.white, fontSize: 32.0, fontWeight: FontWeight.w500)),
                  new Text(strings.appSlogan, style: new TextStyle(color: Colors.white, fontWeight: FontWeight.w200)),
                ],
              )
            )
          )
        ],
      ),
      bottomNavigationBar: new Container // Search school card
      (
        margin: new EdgeInsets.all(8.0),
        child: new Column
        (
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>
          [
            new Container
            (
              margin: new EdgeInsets.symmetric(horizontal: 8.0),
              child: new TextField
              (
                controller: _schoolNameController,
                decoration: new InputDecoration
                (
                  hintText: strings.enterSchoolName
                ),
              ),
            ),
            new Container
            (
              child: new InkWell
              (
                onLongPress: () => Navigator.push(context, new MaterialPageRoute(builder: (_) => new UIOnboarding())),
                child: new RaisedButton
                (
                  onPressed: () => _checkSchoolExists(_schoolNameController.text),
                  color: Colors.black,
                  child: new ListTile
                  (
                    leading: new Icon(Icons.search, color: Colors.white),
                    title: new Text("CERCA SCUOLA", style: new TextStyle(color: Colors.white), textAlign: TextAlign.center),
                  )
                ),
              )
            ),
          ],
        ),
      ),
    );
  }

  Widget _schoolSplashScreen()
  {
    return new Scaffold
    (
      backgroundColor: Colors.deepPurple,
      body: new Center
      (
        child: new Column
        (
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>
          [
            new Text(main.schoolName, style: new TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: 32.0), textAlign: TextAlign.center),
            new Container
            (
              margin: new EdgeInsets.only(top: 16.0),
              child: new Text(main.schoolAddress, style: new TextStyle(color: Colors.white, fontWeight: FontWeight.w300, fontSize: 24.0), textAlign: TextAlign.center),
            )
          ],
        ),
      )
    );
  }

  void _checkSchoolExists(String _schoolName)
  {
    if(_schoolName.trim().length > 0)
    {
      schools.clear();

      showDialog // Show loading dialog
      (
        context: context,
        barrierDismissible: false,
        child: new Dialog
        (
          child: new Container
          (
            margin: new EdgeInsets.all(24.0),
            child: new Row
            (
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>
              [
                new CircularProgressIndicator(),
                new Flexible
                (
                  child: new Container
                  (
                    margin: new EdgeInsets.only(left: 32.0),
                    child: new Text("Un momento, sto cercando la tua scuola..."),
                  )
                )
              ],
            )
          ),
        )
      );

      _checkSchoolExistsTimer = new Timer
      (
        new Duration(seconds: 4 + (_numbersOfTry*3)),
        ()
        {
          _numbersOfTry++;
          Navigator.of(context).pop();

          if(schools.length == 0) _showDialogSchoolNotFound(_schoolName);
          else
          {
            _showDialogMultipleSchoolsFound();
          }
        }
      );

      Firestore.instance.collection(strings.fsSchoolsCollection).snapshots.listen
      (
        (QuerySnapshot documents) => documents.documents.forEach
        (
          (DocumentSnapshot doc)
          {
            if(_checkSchoolName(doc.data["name"].toString().toLowerCase().trim(), _schoolName.toLowerCase().trim()))
            {
              schools.add(new School(doc.data["name"], doc.data["address"], doc.documentID));
              return;
            }
          }
        )
      );
    }
  }

  bool _checkSchoolName(String userSchoolName, String realSchoolName)
  {
    List<String> _userSchoolWords = userSchoolName.toLowerCase().trim().split(" ");
    List<String> _realSchoolWords = realSchoolName.toLowerCase().trim().split(" ");

    int _score = 0;
    for (int i = 0; i < _userSchoolWords.length; i++)
    {
      for (int j = 0; j < _realSchoolWords.length; j++)
      {
        if(_userSchoolWords[i] == _realSchoolWords[j]) _score++;
      }
    }

    print("$userSchoolName == $realSchoolName => $_score");

    if(_score > 0) return true;
    return false;
  }

  void _showDialogMultipleSchoolsFound()
  {
    schools.add(new School("", "", ""));

    showDialog
    (
      context: context,
      child: new AlertDialog
      (
        title: new Text("Choose your school"),
        content: new Container
        (
          child: new ListView.builder
          (
            itemBuilder: (_, int pos)
            {
              if(pos == schools.length - 1)
              {
                return new InkWell
                (
                  onTap: ()
                  {
                    Navigator.of(context).pop();
                    Navigator.push(context, new MaterialPageRoute(builder: (_) => new UIAddNewSchool()));
                  },
                  child: new ListTile
                  (
                    leading: new Icon(Icons.add),
                    title: new Text("Add new school"),
                  ),
                );
              }
              else
              {
                return new InkWell
                (
                  onTap: ()
                  {
                    Navigator.of(context).pop();
                    _loadUIMain(schools[pos]);
                  },
                  child: new ListTile
                  (
                    title: new Text(schools[pos].name),
                    subtitle: new Text(schools[pos].address),
                  ),
                );
              }
            },
            itemCount: schools.length,
          )
        ),
        actions: <Widget>
        [
          new FlatButton
          (
            onPressed: () => Navigator.pop(context),
            child: new Text("ANNULLA"),
          )
        ],
      )
    );
  }

  void _showDialogSchoolNotFound(String _schoolSearched)
  {
    showDialog
    (
      context: context,
      child: new AlertDialog
      (
        title: new Text("\"${_schoolSearched.trim()}\" non trovata"),
        content: new Text("Non abbiamo questa scuola nell'app.\n\nQuesto significa che hai fatto un errore di battitura, o che sei il primo ribelle della tua scuola.\nAssicurati anche che la tua connessione internet funzioni."),
        actions: <Widget>
        [
          new FlatButton
          (
            onPressed: () => Navigator.of(context).pop(),
            child: new Text("HO SBAGLIATTO"),
          ),
          new FlatButton
          (
            onPressed: () { Navigator.of(context).pop(); Navigator.push(context, new MaterialPageRoute(builder: (_) => new UIAddNewSchool())); },
            child: new Text("SONO IL PRIMO"),
          ),
        ],
      )
    );
  }

  _loadUIMain(School choosenSchool) async
  {
    _checkSchoolExistsTimer.cancel();
    _changeImgsTimer.cancel();

    // Set the values for the splash screen
    main.schoolName = choosenSchool.name;
    main.schoolAddress = choosenSchool.address;
    main.schoolID = choosenSchool.schoolId;

    // Save the values
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(strings.prefsSchoolName, choosenSchool.name);
    prefs.setString(strings.prefsSchoolAddress, choosenSchool.address);
    prefs.setString(strings.prefsSchoolId, choosenSchool.schoolId);
    prefs.commit();
    
    // Show the school name on full page for 1 second and go to the ui_main
    setState(() => scanned = true);    
    new Timer(new Duration(seconds: 3), ()
    {
      print(choosenSchool.problems[0].solutions);
      Navigator.of(context).pushReplacement(new MaterialPageRoute(builder: (_) => new UIMain()));
    });

    _loadSchoolProblems(choosenSchool);
  }

  _loadSchoolProblems(School _school)
  {
    Firestore.instance.collection(strings.fsSchoolsCollection + "/" + _school.schoolId + "/problems").snapshots.listen
    (
      (QuerySnapshot documents) => documents.documents.forEach
      (
        (DocumentSnapshot doc)
        {
          _school.problems.add
          (
            new Problem
            (
              title: doc.data["title"],
              desc: doc.data["desc"],
              upvotes: int.parse(doc.data["upvotes"].toString())
            )
          );

          int _pos = _school.problems.length - 1;
          Firestore.instance.collection(strings.fsSchoolsCollection + "/" + _school.schoolId + "/problems/" + doc.documentID + "/solutions").snapshots.listen
          (
            (QuerySnapshot documents) => documents.documents.forEach
            (
              (DocumentSnapshot doc2)
              {
                _school.problems[_pos].solutions.add(new Solution(doc2.data["title"], int.parse(doc2.data["upvotes"].toString())));
              }
            )
          );
        }
      )
    );
  }

  Widget _valutaApp()
  {
    return new Container
    (
      child: new ListTile
      (
        leading: new Image.asset("img/icon.png"),
        title: new Text("Hai 5 secondi per valutare l'app?"),
        trailing: new FlatButton
        (
          onPressed: ()
          {
            _launchPlayStore();
            showDialog
            (
              context: context,
              child: new Dialog
              (
                child: new Container(margin: new EdgeInsets.all(32.0), child: new Text("Grazie!", textAlign: TextAlign.center)),
              )
            );
          },
          child: new Text("PLAY STORE", style: new TextStyle(color: Colors.deepPurpleAccent)),
        ),
      ),
    );
  }

  _launchPlayStore() async
  {
    const url = 'https://play.google.com/store/apps/details?id=com.skuu.einsteinopenday';
    if (await canLaunch(url)) await launch(url);
    else throw 'Could not launch $url';
  }

  Future scanQr() async
  {
    _checkSchoolExists(await BarcodeScanner.scan());
  }
}