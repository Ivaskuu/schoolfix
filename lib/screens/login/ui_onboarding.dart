import 'package:flutter/material.dart';

List<OnboardPage> pages =
[
  new OnboardPage(title: "Join the rebellion", desc: "And list your school problems"),
  new OnboardPage(title: "End the dictature", desc: "And find solutions toghether"),
  new OnboardPage(title: "Scan the QR code in your school to get started"),
  new OnboardPage(title: "You are now ready to use the app"),
];

final PageController pageController = new PageController();

class UIOnboarding extends StatefulWidget
{
  @override
  _UIOnboardingState createState() => new _UIOnboardingState();
}

class _UIOnboardingState extends State<UIOnboarding>
{
  @override
  Widget build(BuildContext context)
  {
    return new Scaffold
    (
      body: new PageView.builder
      (
        itemBuilder: (_, int i) => new ViewOnboardPage(i, pages[i]),
        itemCount: pages.length,
        controller: pageController,
        scrollDirection: Axis.horizontal,
      )
    );
  }
}

class OnboardPage
{
  final String title;
  final String desc;
  final Color color;

  OnboardPage({this.title: "", this.desc: "", this.color: Colors.white});
}

class ViewOnboardPage extends StatelessWidget
{
  final int arrayPos;
  final OnboardPage page;
  ViewOnboardPage(this.arrayPos, this.page);

  @override
  Widget build(BuildContext context)
  {
    return new Container
    (
      padding: new EdgeInsets.all(16.0),
      child: new Column
      (
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>
        [
          new Container
          (
            margin: new EdgeInsets.only(top: 16.0),
            child: new CircleAvatar
            (
              backgroundColor: Colors.pink,
              radius: 100.0,
              child: new Icon(Icons.phone, color: Colors.white, size: 80.0),
            ),
          ),
          new Container
          (
            margin: new EdgeInsets.only(top: 32.0),
            child: new Text(page.title, style: Theme.of(context).textTheme.title.copyWith(color: Colors.black), textAlign: TextAlign.center),
          ),
          new Container
          (
            margin: new EdgeInsets.only(top: 32.0),
            child: new Text(page.desc, textAlign: TextAlign.center),
          ),
          new Container
          (
            margin: new EdgeInsets.only(top: 150.0),
            child: new RaisedButton
            (
              color: Colors.black,
              onPressed: () => pageController.nextPage(duration: new Duration(seconds: 2), curve: new ElasticOutCurve()),
              child: new ListTile
              (
                title: new Text("CONTINUA", style: new TextStyle(color: Colors.white)),
                trailing: new Icon(Icons.arrow_forward, color: Colors.white),
              ),
            ),
          ),
          new Container
          (
            margin: new EdgeInsets.only(top: 16.0),
            child: currentPageCircles(arrayPos)
          )
        ],
      ),
    );
  }

  Widget currentPageCircles(int pos)
  {
    List<Widget> circles = new List();

    for (var i = 0; i < pages.length; i++)
    {
      circles.add(new Container
      (
        margin: new EdgeInsets.all(3.0),
        child: new CircleAvatar
        (
          radius: 4.0,
          backgroundColor: i == pos ? Colors.black : Colors.grey,
        )
      ));
    }

    return new Container
    (
      child: new Row
      (
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: circles
      ),
    );
  }
}