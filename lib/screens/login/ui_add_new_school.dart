import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../classes/school.dart';
import '../../main.dart' as main;
import '../../strings.dart' as strings;
import '../ui_main.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UIAddNewSchool extends StatefulWidget
{
  @override
  _UIAddNewSchoolState createState() => new _UIAddNewSchoolState();
}

class _UIAddNewSchoolState extends State<UIAddNewSchool>
{
  TextEditingController _nameContr = new TextEditingController();
  TextEditingController _addressContr = new TextEditingController();

  @override
  Widget build(BuildContext context)
  {
    return new Scaffold
    (
      appBar: new AppBar
      (
        title: new Text("Add a new school"),
      ),
      body: new Column
      (
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>
        [
          new Image.asset("img/school3.jpeg"),
          new Expanded(child: new Container
          (
            margin: new EdgeInsets.all(16.0),
            child: new Column
            (
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>
              [
                new Flexible
                (
                  child: new Text("We'll fix your school, too, little rebel ;-)", style: new TextStyle(color: Colors.black, fontSize: 32.0, fontWeight: FontWeight.w600))
                ),
                new TextField
                (
                  controller: _nameContr,
                  decoration: new InputDecoration
                  (
                    hintText: "Your school name"
                  ),
                ),
                new TextField
                (
                  controller: _addressContr,
                  decoration: new InputDecoration
                  (
                    hintText: "Your school address"
                  ),
                ),
              ],
            ),
          ))
        ],
      ),
      bottomNavigationBar: new Container
      (
        margin: new EdgeInsets.all(16.0),
        child: new RaisedButton
        (
          onPressed: ()
          {
            Firestore.instance.collection("schools").document().setData({ "name": _nameContr.text, "address": _addressContr.text }); // Add the new school
            _loadUIMain(_nameContr.text, _addressContr.text);
          },
          color: Colors.black,
          child: new ListTile
          (
            title: new Text("LET'S FIX MY SCHOOL", style: new TextStyle(color: Colors.white), textAlign: TextAlign.center),
            trailing: new Icon(Icons.done, color: Colors.white),
          )
        )
      ),
    );
  }

  _loadUIMain(String _name, String _add) async
  {
    // Set the values for the splash screen
    main.schoolName = _nameContr.text;
    main.schoolAddress = _addressContr.text;
    main.schoolID = "";

    // Save the values
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(strings.prefsSchoolName, _name);
    prefs.setString(strings.prefsSchoolAddress, _add);
    prefs.setString(strings.prefsSchoolId, "");
    prefs.commit();

    Navigator.of(context).pushReplacement(new MaterialPageRoute(builder: (_) => new UIMain()));
  }
}