import 'package:flutter/material.dart';
import 'school/ui_lista.dart';
import 'login/ui_login.dart';
import '../main.dart' as main;
import '../strings.dart' as strings;
import 'package:shared_preferences/shared_preferences.dart';

class UIMain extends StatefulWidget
{  
  @override
  _UIMainState createState() => new _UIMainState();
}

class _UIMainState extends State<UIMain>
{
  @override
  Widget build(BuildContext context)
  {
    return new Scaffold
    (
      appBar: new AppBar
      (
        elevation: 0.0,
        leading: new Icon(Icons.school, color: Colors.white),
        title: new Column
        (
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>
          [
            new Text(main.schoolName, style: new TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: 20.0), overflow: TextOverflow.ellipsis),
            new Text(main.schoolAddress, style: new TextStyle(color: Colors.white, fontWeight: FontWeight.w300, fontSize: 12.0), overflow: TextOverflow.ellipsis),
          ]
        ),
        actions: <Widget>
        [
          new IconButton
          (
            onPressed: () => logOut(),
            icon: new Icon(Icons.exit_to_app),
            tooltip: "Cambia scuola",
          )
        ],
      ),
      body: new UILista(),
    );
  }

  void logOut()
  {
    SharedPreferences.getInstance().then((prefs)
    {
      prefs.remove(strings.prefsSchoolName);
      prefs.remove(strings.prefsSchoolAddress);
      prefs.remove(strings.prefsSchoolId);
      prefs.commit();

      Navigator.of(context).pushReplacement(new MaterialPageRoute(builder: (_) => new UILogin()));
    });
  }
}