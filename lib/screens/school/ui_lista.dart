import 'package:flutter/material.dart';
import 'ui_add_problem.dart';
import 'ui_view_problem.dart';
import 'dart:async';

import '../../classes/school.dart';
import '../../classes/problem.dart';
import '../../classes/solution.dart';
import '../../strings.dart' as strings;
import '../../main.dart' as main;

import 'package:cloud_firestore/cloud_firestore.dart';

class UILista extends StatefulWidget
{
  List<TileProblema> problemi = new List<TileProblema>();
  List<TileProblema> problemiRisolti = new List<TileProblema>();

  @override
  _UIListaState createState() => new _UIListaState();
}

class _UIListaState extends State<UILista>
{
  @override
  void initState()
  {
    super.initState();
    
    _loadSchoolProblems();
    /*new Timer(new Duration(milliseconds: 10), () => showDialog // Show loading dialog
    (
      context: context,
      barrierDismissible: false,
      child: new Dialog
      (
        child: new Container
        (
          margin: new EdgeInsets.all(24.0),
          child: new Row
          (
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>
            [
              new CircularProgressIndicator(),
              new Flexible
              (
                child: new Container
                (
                  margin: new EdgeInsets.only(left: 32.0),
                  child: new Text("Un momento per favore..."),
                )
              )
            ],
          )
        ),
      )
    ));*/
    new Timer(new Duration(seconds: 3), () => setState(()
    {
      for (int i = 0; i < main.school.problems.length; i++)
      {
        if(main.school.problems[i].resolved) widget.problemiRisolti.add(new TileProblema(prob: main.school.problems[i], upvoted: false));
        else widget.problemi.add(new TileProblema(prob: main.school.problems[i], upvoted: false));
      }

      if(widget.problemi.length > 0) { widget.problemi[widget.problemi.length - 1].lastTile = true; }
      widget.problemi[0].prob.resolved = true;

      if(widget.problemiRisolti.length > 0) widget.problemiRisolti[widget.problemiRisolti.length - 1].lastTile = true;

      //Navigator.pop(context);
    }));
  }

  @override
  Widget build(BuildContext context)
  {
    return new Container
    (
      color: Colors.deepPurple,
      child: new ListView
      (
        scrollDirection: Axis.vertical,
        children: <Widget>
        [
          new Container
          (
            height: 180.0,
            child: new Column
            (
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>
              [
                new Text(main.school.problems.length.toString() ?? "...", style: new TextStyle(color: Colors.white, fontSize: 52.0, fontWeight: FontWeight.w600)),
                new Text("problemi", style: new TextStyle(color: Colors.white)),
                new Container
                (
                  margin: new EdgeInsets.only(top: 24.0),
                  child: new RaisedButton
                  (
                    onPressed: () => Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new UIAddProblem())),
                    child: new Text("+ NUOVO PROBLEMA", style: new TextStyle(color: Colors.white)),
                    color: Colors.deepPurpleAccent,
                  ),
                )
              ],
            )
          ),
          new Container
          (
            margin: new EdgeInsets.only(top: 24.0, left: 10.0, bottom: 4.0),
            child: new Text("Lista problemi", style: new TextStyle(color: Colors.white, fontWeight: FontWeight.w700, fontSize: 15.0)),
          ),
          new Card
          (
            child: new Container
            (
              margin: new EdgeInsets.symmetric(vertical: 8.0),
              child: new Column
              (
                mainAxisAlignment: MainAxisAlignment.start,
                children: widget.problemi.length > 0 ? widget.problemi : [new ListTile(subtitle: new Text("Nothing to show here"))],
              )
            )
          ),
          new Container
          (
            margin: new EdgeInsets.only(top: 24.0, left: 10.0, bottom: 4.0),
            child: new Text("Problemi risolti", style: new TextStyle(color: Colors.white, fontWeight: FontWeight.w700, fontSize: 15.0)),
          ),
          new Card
          (
            child: new Container
            (
              margin: new EdgeInsets.symmetric(vertical: 8.0),
              child: new Column
              (
                mainAxisAlignment: MainAxisAlignment.start,
                children: widget.problemiRisolti.length > 0 ? widget.problemiRisolti : [new ListTile(subtitle: new Text("Nothing to show here"))],
              )
            )
          ),
        ],
      )
    );
  }

  _loadSchoolProblems()
  {
    Firestore.instance.collection(strings.fsSchoolsCollection + "/" + main.school.schoolId + "/problems").snapshots.listen
    (
      (QuerySnapshot documents) => documents.documents.forEach
      (
        (DocumentSnapshot doc)
        {
          main.school.problems.add
          (
            new Problem
            (
              title: doc.data["title"],
              desc: doc.data["desc"],
              upvotes: int.parse(doc.data["upvotes"].toString())
            )
          );

          int _pos = main.school.problems.length - 1;
          Firestore.instance.collection(strings.fsSchoolsCollection + "/" + main.school.schoolId + "/problems/" + doc.documentID + "/solutions").snapshots.listen
          (
            (QuerySnapshot documents) => documents.documents.forEach
            (
              (DocumentSnapshot doc2)
              {
                main.school.problems[_pos].solutions.add(new Solution(doc2.data["title"], int.parse(doc2.data["upvotes"].toString())));
              }
            )
          );
        }
      )
    );
  }
}

class TileProblema extends StatelessWidget
{
  Problem prob;
  bool upvoted;
  bool lastTile;

  TileProblema({this.prob, this.upvoted: false, this.lastTile: false});

  @override
  Widget build(BuildContext context)
  {
    return new Column
    (
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>
      [
        new InkWell
        (
          onTap: () => Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new UIViewProblem())),
          child: new ListTile
          (
            isThreeLine: true,
            leading: new Center
            (
              child: new Column
              (
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>
                [
                  new Icon(Icons.chat),
                  new Text("${prob.solutions.length}", style: Theme.of(context).textTheme.title),
                ],
              )
            ),
            title: new Text("${prob.title}"),
            subtitle: new Text("${prob.desc}", maxLines: 1, overflow: TextOverflow.ellipsis),
            trailing: !prob.resolved ? new Row
            (
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>
              [
                upvoted ?
                  new CircleAvatar
                  (
                    backgroundColor: Colors.green,
                    radius: 27.0,
                    child: new InkWell
                    (
                      onTap: () => null,
                      child: new Text("${prob.upvotes}", style: Theme.of(context).textTheme.title.copyWith(color: Colors.white)),
                    )
                  )
                :
                  new Column
                  (
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>
                    [
                      new Text("${prob.upvotes}", style: Theme.of(context).textTheme.title),
                      new IconButton
                      (
                        icon: new Icon(Icons.arrow_upward),
                        onPressed: () => null,
                      ),
                    ],
                  )
              ],
            )
            : new Icon(Icons.done, color: Colors.green),
          )
        ),
        lastTile != null ? new Container() : new Divider(),
      ],
    );
  }
}