import 'package:flutter/material.dart';

class UIAddProblem extends StatelessWidget
{
  @override
  Widget build(BuildContext context)
  {
    return new Scaffold
    (
      appBar: new AppBar
      (
        actions: [ new IconButton(icon: new Icon(Icons.close, color: Colors.black38), onPressed: () => Navigator.of(context).pop()) ],
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      body: new Container
      (
        padding: new EdgeInsets.symmetric(horizontal: 16.0),
        child: new Column
        (
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>
          [
            new Container
            (
              margin: new EdgeInsets.only(bottom: 16.0),
              child: new Text
              (
                "Aggiungi un nuovo problema",
                style: Theme.of(context).textTheme.title.copyWith(color: Colors.black, fontSize: 36.0))
            ),
            new TextField
            (
              decoration: new InputDecoration
              (
                hintText: "Il titolo del problema"
              ),
            ),
            new TextField
            (
              maxLines: 3,
              decoration: new InputDecoration
              (
                hintText: "Descrizione del problema"
              ),
            ),
            new Container
            (
              margin: new EdgeInsets.symmetric(vertical: 16.0),
              child: new Card
              (
                //onPressed: () => null,
                child: new Column
                (
                  children: <Widget>
                  [
                    new InkWell
                    (
                      onTap: () => null,
                      child: new ListTile
                      (
                        leading: new Icon(Icons.photo_camera, color: Colors.black),
                        title: new Text("Aggiungi una foto")
                      ),
                    ),
                    new Divider(),
                    new InkWell
                    (
                      onTap: () => null,
                      child: new ListTile
                      (
                      leading: new Icon(Icons.share, color: Colors.blue),
                      title: new Text("Condividi il problema")
                      ),
                    ),
                  ],
                )
              )
            ),
            new Container
            (
              margin: new EdgeInsets.only(top: 16.0),
              child: pillButton
              (
                child: new Container
                (
                  margin: new EdgeInsets.symmetric(vertical: 16.0),
                  child: new Text("Aggiungi il problema", style: new TextStyle(color: Colors.white)),
                ),
                onPressed: () => Navigator.of(context).pop()
              ),
            )
          ],
        )
      )
    );
  }

  Widget pillButton({Widget child, VoidCallback onPressed})
  {
    return new Container
    (
      margin: new EdgeInsets.symmetric(vertical: 8.0, horizontal: 32.0),
      child: new Material
      (
        elevation: 8.0,
        borderRadius: new BorderRadius.circular(32.0),
        child: new InkWell
        (
          onTap: () => onPressed(),
          child: new Container
          (
            decoration: new BoxDecoration
            (
              borderRadius: new BorderRadius.circular(32.0),
              gradient: new LinearGradient
              (
                colors: <Color> [new Color.fromARGB(255, 93, 107, 232), new Color.fromARGB(255, 89, 182, 191)]
              ),
            ),
            child: new Center(child: child),
            padding: new EdgeInsets.symmetric(horizontal: 48.0),
          ),
        )
      )
    );
  }
}