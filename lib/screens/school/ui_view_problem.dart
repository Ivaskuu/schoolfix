import 'package:flutter/material.dart';
import 'ui_add_problem.dart';
import '../../classes/problem.dart';

class UIViewProblem extends StatefulWidget
{
  @override
  _UIViewProblemState createState() => new _UIViewProblemState();
}

class _UIViewProblemState extends State<UIViewProblem>
{
  @override
  Widget build(BuildContext context)
  {
    return new Scaffold
    (
      backgroundColor: Colors.deepPurple,
      body: new Stack
      (
        children: <Widget>
        [
          new Container // Bg img
          (
            decoration: new BoxDecoration
            (
              image: new DecorationImage
              (
                image: new AssetImage("img/tags.jpg"),
                fit: BoxFit.cover,
                colorFilter: new ColorFilter.mode(Colors.black87, BlendMode.darken)
              )
            ),
          ),
          new Align // Header and appbar
          (
            alignment: FractionalOffset.topCenter,
            child: new Column
            (
              mainAxisSize: MainAxisSize.min,
              children: <Widget>
              [
                new AppBar
                (
                  elevation: 0.0,
                  backgroundColor: Colors.black12,
                  actions: <Widget>
                  [
                    new IconButton
                    (
                      onPressed: () => null,
                      icon: new Icon(Icons.more_vert, color: Colors.white),
                    )
                  ],
                ),
                new Flexible
                (
                  child: new Container
                  (
                    margin: new EdgeInsets.only(left: 16.0, right: 16.0, bottom: 16.0),
                    child: new Text("I bagni degli maschi sono sempre sporchi", style: new TextStyle(color: Colors.white, fontSize: 42.0, fontWeight: FontWeight.w600)),
                  )
                ),
                new Container
                (
                  margin: new EdgeInsets.symmetric(horizontal: 16.0),
                  child: new Row
                  (
                    children: <Widget>
                    [
                      new Flexible
                      (
                        child: new Container
                        (
                          margin: new EdgeInsets.only(right: 16.0),
                          child: new Text
                          (
                            "Questa è una descrizione molto lunga del problema che dovrebbe normalmente andare su piu linee se tutto va bene e che funzioni con un listtile",
                            style: new TextStyle(color: Colors.white)
                          ),
                        )
                      ),
                      new RaisedButton
                      (
                        onPressed: () => null,
                        color: Colors.white,
                        child: new Container
                        (
                          padding: new EdgeInsets.symmetric(horizontal: 8.0, vertical: 16.0),
                          child: new Row
                          (
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>
                            [
                              new Icon(Icons.keyboard_arrow_up),
                              new Container
                              (
                                margin: new EdgeInsets.only(left: 14.0),
                                child: new Text("34")
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                /*new Container
                (
                  margin: new EdgeInsets.only(top: 24.0, bottom: 24.0),
                  child: new FloatingActionButton
                  (
                    onPressed: () => null,
                    mini: true,
                    backgroundColor: Colors.white,
                    child: new Icon(Icons.keyboard_arrow_down, color: Colors.black),
                  ),
                )*/
              ]
            )
          ),
          new Align // Solutions
          (
            alignment: FractionalOffset.bottomCenter,
            child: new SizedBox.fromSize
            (
              size: new Size.fromHeight(200.0),
              child: new Container
              (
                child: new ListView
                (
                  scrollDirection: Axis.horizontal,
                  children: <Widget>
                  [
                    new Card
                    (
                      child: new GestureDetector
                      (
                        onVerticalDragUpdate: (DragUpdateDetails details) => details.primaryDelta < -21 ? Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new UIAddProblem())) : null,
                        child: new InkWell
                        (
                          onTap: () => Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new UIViewProblem())),
                          child: new SizedBox.fromSize
                          (
                            size: new Size.fromWidth(300.0),
                            child: new Container
                            (
                              child: new ListTile
                              (
                                title: new Text("Soluzione #1"),
                                subtitle: new Text("Pulire prima di ogni intervallo il bagno", style: new TextStyle(color: Colors.black, fontSize: 24.0, fontWeight: FontWeight.w600)),
                              )
                            )
                          ),
                        ),
                      )
                    ),
                    new Card
                    (
                      child: new GestureDetector
                      (
                        onVerticalDragUpdate: (DragUpdateDetails details) => details.primaryDelta < -21 ? Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new UIAddProblem())) : null,
                        child: new InkWell
                        (
                          onTap: () => Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new UIAddProblem())),
                          child: new SizedBox.fromSize
                          (
                            size: new Size.fromWidth(300.0),
                            child: new Container
                            (
                              child: new ListTile
                              (
                                title: new Text("Soluzione #2"),
                                subtitle: new Text("Pulire prima di ogni intervallo il bagno", style: new TextStyle(color: Colors.black, fontSize: 24.0, fontWeight: FontWeight.w600)),
                              )
                            )
                          ),
                        ),
                      )
                    ),
                    new Card
                    (
                      child: new GestureDetector
                      (
                        onVerticalDragUpdate: (DragUpdateDetails details) => details.primaryDelta < -21 ? Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new UIAddProblem())) : null,
                        child: new InkWell
                        (
                          onTap: () => Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new UIAddProblem())),
                          child: new SizedBox.fromSize
                          (
                            size: new Size.fromWidth(300.0),
                            child: new Container
                            (
                              child: new ListTile
                              (
                                title: new Text("Aggiungi una soluzione", style: new TextStyle(color: Colors.black, fontSize: 24.0, fontWeight: FontWeight.w600)),
                                subtitle: new Icon(Icons.add, size: 80.0, color: Colors.black),
                              )
                            )
                          ),
                        ),
                      )
                    ),
                  ],
                )
              ),
            )
          )
        ],
      ),
    );
  }
}