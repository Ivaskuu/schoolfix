import 'solution.dart';

class Problem
{
  final String title;
  final String desc;
  int upvotes;
  List<Solution> solutions;
  bool resolved;

  Problem({this.title: "", this.desc: "", this.upvotes: 0, this.solutions, this.resolved: false})
  {
    solutions = new List<Solution>();
  }
}