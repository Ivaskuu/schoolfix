import 'problem.dart';

class School
{
  final String name;
  final String address;
  final String schoolId;
  List<Problem> problems;

  School(this.name, this.address, this.schoolId, {this.problems})
  {
    problems = new List<Problem>();
  }
}